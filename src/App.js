

import React ,{Component} from 'react';
import config from './config.json'
import styles from './main.css'
import { Link } from 'react-router-dom';
import {DatePicker} from 'antd'



class App extends Component {
  render() {
    console.log(config)
    return (
      <div>
        <h1>App</h1>
        <DatePicker/>
        <div className={styles.root}>
          {config.greetText}
        </div>
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/pone">page1</Link></li>
          <li><Link to="/ptwo">page2</Link></li>
        </ul>
        {this.props.children}
      </div>
    );
  }
}

export default App