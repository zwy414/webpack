// const greeter = require('./Greeter');

// document.getElementById('root').appendChild(greeter())

import React from 'react';
import ReactDOM,{render} from 'react-dom';
import App from './App'
import {  HashRouter, Route,  Link, Switch } from 'react-router-dom';
import Home from './Components/Home'
import Pone from './Components/Pone'
import Ptwo from './Components/Ptwo'


ReactDOM.render(<HashRouter>
    <App>
        <Route exact path="/" component={Home} />
        <Route path="/pone" component={Pone} />
        <Route path="/ptwo" component={Ptwo} />
    </App>
  </HashRouter>,
  document.getElementById('root')
)


